﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace COMapViewer.Files
{
    public class ScenePiece
    {
        public string aniKey;
        public ScenePiece(string _aniKey)
        {
            aniKey = _aniKey;
        }
        public Texture2D GetTexture(string aniFileName)
        {
            return AniCollectionManager.GetTexture2D(aniFileName, aniKey);
        }
    }
    public class SceneFile
    {
        uint fileCellX;
        uint fileCellY;

        string fileScenePath;

        public SceneFile(BinaryReader reader)
        {
            fileScenePath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(260));
            fileCellX = reader.ReadUInt32();
            fileCellY = reader.ReadUInt32();
            Console.WriteLine("SceneCellStart: {0},{1}", fileCellX, fileCellY);
            LoadScene(fileScenePath);
        }

        private void LoadScene(string scenePath)
        {
            using (BinaryReader reader = new BinaryReader(File.OpenRead(Game1.BaseClientPath + scenePath)))
            {
                uint partCount = reader.ReadUInt32();
                for(int i = 0; i < partCount; i++)
                {
                    string partPath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(256));
                    string partName = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(64));
                    int partOriginX = reader.ReadInt32();
                    int partOriginY = reader.ReadInt32();
                    int partFrameInterval = reader.ReadInt32();
                    int partWidth = reader.ReadInt32();
                    int partHeight = reader.ReadInt32();
                    int partThickeness = reader.ReadInt32();
                    int partOffsetX = reader.ReadInt32();
                    int partOffsetY = reader.ReadInt32();
                    int offsetElevation = reader.ReadInt32();

                    for(int y = 0; y < partHeight; y++)
                    {
                        for(int x = 0; x < partWidth; x++)
                        {
                            uint partInvalidMovement = reader.ReadUInt32();
                            uint partSurfaceType = reader.ReadUInt32();
                            int partelevation = reader.ReadInt32();
                        }
                    }

                    Console.WriteLine("Path:{0} Name:{1} ox,oy:({2},{3}) FrameInt:{4} w,h:({5},{6}) Thickness: {7}" +
                        " offX,y:({8},{9}) offElve:{10}", partPath, partName, partOriginX, partOriginY, partFrameInterval,
                        partWidth, partHeight, partThickeness, partOffsetX, partOffsetY, offsetElevation);
                }

                if (reader.BaseStream.Position != reader.BaseStream.Length)
                    Console.WriteLine("[SceneFile] [LoadScene] Missing Scene file information");
            }
        }
    }
}
