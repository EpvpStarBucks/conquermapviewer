﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using MonoGame.Extended;

namespace COMapViewer.Files
{
    public class PuzzleFile
    {
        public Size PUZZLESIZE;
        private ushort[,] m_aniPieces;
        
        public Size m_infoSize;
        byte m_dwVersion;

        private string _aniFileName;


        public uint RollSpeedX = 0;
        public uint RollSpeedY = 0;

        public PuzzleFile() { }
        public PuzzleFile(string fileName)
        {
            Load(fileName);
        }

        public Texture2D GetTexture(int x, int y)
        {
            return AniCollectionManager.GetTexture2D(_aniFileName, string.Format("Puzzle{0}",m_aniPieces[x, y]));
        }
        private bool Load(string fileName)
        {
            using (BinaryReader reader = new BinaryReader(File.OpenRead(Game1.BaseClientPath + fileName)))
            {
                string szHeader =  BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(8));

                if (szHeader == "PUZZLE")
                    m_dwVersion = 1;
                else if (szHeader == "PUZZLE2")
                    m_dwVersion = 2;

                _aniFileName = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(256));
                AniCollectionManager.LoadAniFile(_aniFileName);

                this.m_infoSize = new Size(reader.ReadInt32(), reader.ReadInt32());
                m_aniPieces = new ushort[m_infoSize.Width, m_infoSize.Height];

                for (int y = 0; y < m_infoSize.Height; y++)
                {
                    for (int x = 0; x < m_infoSize.Width; x++)
                    {
                        m_aniPieces[x, y] = reader.ReadUInt16();
                        if (PUZZLESIZE.Width == 0)
                            PUZZLESIZE = AniCollectionManager.GetTexture2DSize(_aniFileName, "Puzzle" + m_aniPieces[x, y]);
                    }
                }

                if (m_dwVersion == 2) {
                    RollSpeedX = reader.ReadUInt32();
                    RollSpeedY = reader.ReadUInt32();
                }
                //Loading Puzzles.
                Debug.WriteLine("[Puzzle:Load] AniFile: {4} Version:{0}, (Width,Height)=({1},{2}), usPuzzleAmount:{3}, RollSpeed:({5},{6})", m_dwVersion, m_infoSize.Width, m_infoSize.Height, m_infoSize.Width * m_infoSize.Height, _aniFileName, RollSpeedX, RollSpeedY);
                return true;
            }
        }

        public void Process(Vector2 posUpdate)
        {
        }
        public void Draw(SpriteBatch spriteBatch,  Rectangle drawWindow)
        {
            int numPiecesX = (int)(drawWindow.Width / PUZZLESIZE.Width) + 2;
            int numPiecesY = (int)(drawWindow.Height / PUZZLESIZE.Width) + 2;

            int startPieceX = (int)(drawWindow.X / PUZZLESIZE.Width);
            int startPieceY = (int)(drawWindow.Y / PUZZLESIZE.Width);

            int offsetX = -1 * (int)(drawWindow.X % PUZZLESIZE.Width);
            int offsetY = -1 * (int)(drawWindow.Y % PUZZLESIZE.Width);

            int drawPositionX = offsetX;
            int drawPositionY = offsetY;

            int drawcount = 0;
            for (int x = startPieceX; x < (startPieceX+ numPiecesX); x++)
            {
                for (int y = startPieceY; y < (startPieceY +numPiecesY); y++)
                {
                    if (x < m_infoSize.Width && y < m_infoSize.Height)
                    {
                        Texture2D tex = GetTexture(x, y);
                        if(tex != null)
                            spriteBatch.Draw(tex, new Vector2(drawPositionX, drawPositionY), Microsoft.Xna.Framework.Color.AliceBlue);
                        drawPositionY += (int)PUZZLESIZE.Width;
                        drawcount++;
                    }
                }
                drawPositionX += (int)PUZZLESIZE.Width;
                drawPositionY = offsetY;
            }
            
            //Console.WriteLine("Drawing Puzzle: {0} Count: {1}", posShow, drawcount);
        }
    }
}
