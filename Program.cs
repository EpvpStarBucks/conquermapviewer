﻿using System;
using System.Windows.Forms;

namespace COMapViewer
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new DataView());
            using (var game = new Game1())
                game.Run();
        }
    }
}
