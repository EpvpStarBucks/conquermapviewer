﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COMapViewer
{
    public partial class MapDataView : Form
    {
        DMap.DataMap refGameMap;
        List<int> selectedRows = new List<int>();
        public MapDataView()
        {
            InitializeComponent();
        }
        public void SetGameMap(DMap.DataMap gameMap)
        {
            refGameMap = gameMap;
            terrainObjBS = new BindingSource();
            terrainObjBS.DataSource = refGameMap.terrainObjects;
            dataGridView1.DataSource = terrainObjBS;
        }
        private void btnToggleGrid_Click(object sender, EventArgs e)
        {
            if(refGameMap != null)
                refGameMap.DrawGrid = !refGameMap.DrawGrid;
        }

        private void cbDrawGrid_CheckedChanged(object sender, EventArgs e)
        {
            if (refGameMap != null)
                refGameMap.DrawGrid = (sender as CheckBox).Checked;
        }

        private void cbDrawTObjs_CheckedChanged(object sender, EventArgs e)
        {
            if (refGameMap != null)
                refGameMap.DrawTerrainObjects = (sender as CheckBox).Checked;
        }

        private void MapDataView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing) { e.Cancel = true; this.Visible = false; }
        }

        private void dataGridView1_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if(this.dataGridView1.Columns[e.ColumnIndex].Name == "location" || this.dataGridView1.Columns[e.ColumnIndex].Name == "offset")
            {
                if(e != null)
                    if(e.Value != null)
                    {
                        Debug.WriteLine("Attempting to parse");
                        //int frameAmount = int.Parse(new Regex(@"\d+").Match(reader.ReadLine()).Value);
                        MatchCollection matches = new Regex(@"\d+").Matches(e.Value.ToString());
                        e.Value = new Microsoft.Xna.Framework.Point(int.Parse(matches[0].Value), int.Parse(matches[1].Value));

                        e.ParsingApplied = true;

                    }
            }
            if (this.dataGridView1.Columns[e.ColumnIndex].Name == "size")
            {
                if (e != null)
                    if (e.Value != null)
                    {
                        Debug.WriteLine("Attempting to parse");
                        //int frameAmount = int.Parse(new Regex(@"\d+").Match(reader.ReadLine()).Value);
                        MatchCollection matches = new Regex(@"\d+").Matches(e.Value.ToString());
                        e.Value = new MonoGame.Extended.Size(int.Parse(matches[0].Value), int.Parse(matches[1].Value));

                        e.ParsingApplied = true;

                    }
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection col = dataGridView1.SelectedRows;

            List<int> newSel = (from DataGridViewRow r in col select r.Index).ToList();
            foreach(int oidx in selectedRows)
            {
                if (!newSel.Contains(oidx))
                {
                    ((DMap.TerrainObject)dataGridView1.Rows[oidx].DataBoundItem).selected = false;
                }
            }
            selectedRows = newSel;
            foreach(int idx in selectedRows)
            {
                ((DMap.TerrainObject)dataGridView1.Rows[idx].DataBoundItem).selected = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            BinaryWriter writer;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "dmap files (*.dmap)|*.dmap|AllFiles (*.*)|*.*";
            sfd.FilterIndex = 0;
            sfd.RestoreDirectory = true;

            if(sfd.ShowDialog() == DialogResult.OK)
            {
                if((writer = new BinaryWriter(sfd.OpenFile())) != null)
                {
                    if (refGameMap.SaveDataMap(writer))
                        MessageBox.Show("DMap successfully saved", "DMap Writer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("DMap failed saved", "DMap Writer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    writer.Close();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            List<int> delRow = new List<int>(selectedRows);
            foreach (int idx in delRow)
            {
                selectedRows.Remove(idx);
                terrainObjBS.RemoveAt(idx);
            }
            selectedRows = new List<int>();
        }
    }
}
