﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMapViewer
{
    public static class BaseFunc
    {
        public static string ReadCString(Encoding encoding, byte[] cstring)
        {
            int nullIndex = Array.IndexOf(cstring, (byte)0);
            nullIndex = (nullIndex == -1) ? cstring.Length : nullIndex;
            return encoding.GetString(cstring, 0, nullIndex);
        }
    }
}
