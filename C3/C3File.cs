﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;

namespace COMapViewer.C3
{
    public static class C3File
    {


        public enum C3PartType
        {
            //Physics
            PHY = 0x20594850,
            PHY3 = 0x33594850,
            PHY4 = 0x34594850,
            //Camera
            CAME = 0x454D4143,

            //KeyFrames
            KKEY = 0x59454B4B,
            XKEY = 0x59454B58,
            ZKEY = 0x59454B5A,
            
            //Motion
            MOTI = 0x49544F4D,

            //Light
            OMNI = 0x484E4D4F,

            //Particle
            PTC3 = 0x33435450,
            PTCL = 0x4C435450,
            PTCX = 0x58435450,

            //Shape
            SHAP = 0x50414853,
            SHP2 = 0x32504853,

            //ShapeMotion
            SMOT = 0x544F4D53,

            STEP = 0x50455453,
            _2SID = 0x44495332
        }
        public static C3_Phy LoadFile(string fileName)
        {
            if(!File.Exists(fileName))
            {
                Console.WriteLine("File doesn't exist");
                return null ;
            }
            using(BinaryReader reader = new BinaryReader(File.OpenRead(fileName)))
            {
                C3_Phy myphy = null;
                C3_Moti myMoti = null;
                string magic = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(16));
                Console.WriteLine("Magic: " + magic);
                bool exitLoop = false;
                while (reader.BaseStream.Position != reader.BaseStream.Length)
                {
                    int partType = reader.ReadInt32();
                    int partLEn = reader.ReadInt32();
                    Console.WriteLine("Part Type and Len: {0:X} and {1}", partType, partLEn);
                    switch ((C3PartType)partType)
                    {
                        case C3PartType.PHY:
                            {
                                myphy = new C3_Phy(reader);
                                break;
                            }
                        case C3PartType.MOTI:
                            {
                                myphy.motion = new C3_Moti(reader);
                                break;
                            }
                        case C3PartType.CAME:
                            {
                                C3_Camera myCame = new C3_Camera(reader);
                            }
                            break;
                        default:
                            Console.WriteLine("Unsupported C3 Part: {0:X}",partType);
                            exitLoop = true;
                            break;
                    }
                    if (exitLoop)
                        break;
                }
                if (reader.BaseStream.Position == reader.BaseStream.Length)
                    Console.WriteLine("Read Complete C3 File");
                    return myphy;
            }
        }
        public  static PhyVertex ReadPhyVertex(BinaryReader reader)
        {
            PhyVertex phyVertex = new PhyVertex();
            for (int i = 0; i < phyVertex.pos.Length; i++)
                phyVertex.pos[i] = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            phyVertex.u = reader.ReadSingle();
            phyVertex.v = reader.ReadSingle();
            phyVertex.color = reader.ReadUInt32();
            phyVertex.index[0] = reader.ReadUInt32();
            phyVertex.index[1] = reader.ReadUInt32();
            phyVertex.weight[0] = reader.ReadSingle();
            phyVertex.weight[1] = reader.ReadSingle();
            phyVertex.ToString();
            return phyVertex;
        }
    }
}
