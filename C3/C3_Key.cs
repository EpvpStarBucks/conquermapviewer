﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMapViewer.C3
{
    public struct C3_Frame
    {
        public int frame;
        public float fParam;
        public bool bParam;
        public int nParam;
    }
    public class C3_Key
    {
        public uint countAlphas;
        public C3_Frame[] alphas;

        public uint countDraws;
        public C3_Frame[] draws;

        public uint countChangeTexs;
        public C3_Frame[] changeTexs;
        public bool ProcessAlpha(uint frame, ref float returnVal)
        {

            int sIndex = -1, eIndex = -1;
            for (int n = 0; n < this.countAlphas; n++)
            {
                if (alphas[n].frame <= frame)
                    if (sIndex == -1 || n > sIndex)
                        sIndex = n;
                if (alphas[n].frame > frame)
                    if (eIndex == -1 || n < eIndex)
                        eIndex = n;
            }
            if (sIndex == -1 && eIndex > 1)
                returnVal = alphas[eIndex].fParam;
            else if (sIndex > -1 && eIndex == -1)
                returnVal = alphas[sIndex].fParam;
            else if (sIndex > -1 && eIndex > -1)
                returnVal = alphas[sIndex].fParam + (((float)frame - alphas[sIndex].frame) / ((float)(alphas[eIndex].frame - alphas[sIndex].frame))) * (alphas[eIndex].fParam - alphas[sIndex].fParam);
            else
                return false;
            return true;
        }

        public bool ProcessDraw(uint frame, ref bool returnVal)
        {
            for(int n = 0; n < countDraws; n++)
            {
                if(draws[n].frame == frame)
                {
                    returnVal = draws[n].bParam;
                    return true;
                }
            }
            return false;
        }

        public bool ProcessChangeTexture(uint frame, ref int returnVal)
        {
            for (int n = 0; n < countChangeTexs; n++)
            {
                if (draws[n].frame == frame)
                {
                    returnVal = draws[n].nParam;
                    return true;
                }
            }
            return false;
        }
    }

}
