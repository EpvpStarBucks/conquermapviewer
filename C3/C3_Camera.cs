﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace COMapViewer.C3
{
    public class C3_Camera
    {
        public string name;             // Camera name
        public Vector3[] from;   // Position for each frame
        public Vector3[] to;     // Target for each frame
        public float near;			    // Near plane
        public float far;			    // Far plane
        public float fov;			    // Field of view (angle in the 'y' direction)
        public uint framesCount;      // Amount of frames
        public int nFrame;              // Current frame number
        private byte[] n, b;            // When saving, buffers to store data
        private int p;

        public C3_Camera(BinaryReader reader)
        {
            LoadFile(reader);
        }
        public void LoadFile(BinaryReader reader)
        {
            name = ASCIIEncoding.ASCII.GetString(reader.ReadBytes(reader.ReadInt32()));
            fov = reader.ReadSingle();
            Console.WriteLine("Fov: " + fov);
            framesCount = reader.ReadUInt32();
            from = new Vector3[framesCount];
            for (int i = 0; i < framesCount; i++)
                from[i] = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

            to = new Vector3[framesCount];
            for (int i = 0; i < framesCount; i++)
                to[i] = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

        }

    }
}
