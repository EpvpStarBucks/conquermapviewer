﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMapViewer.C3
{
    public struct C3KeyFrame
    {
        public uint pos;
        public int bonesCount;
        public Matrix [] matrix;     // size = boneCount
    }
}
