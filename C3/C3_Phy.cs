﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static COMapViewer.C3.C3File;

namespace COMapViewer.C3
{

    public class PhyVertex
    {
        public Vector3[] pos = new Vector3[4];
        public float u, v;
        public uint color;
        public uint[] index = new uint[2];
        public float[] weight = new float[2];
        public override string ToString()
        {
            return string.Format("PhyVertex: {0}, {1}, {2}, {3}, {4}, {5}, {6}", u, v, color, index[0], index[1], weight[0], weight[1]);
        }
    }
    public class C3_Phy
    {

        public string modelName;
        public uint blendCount;       // how many bones affect each vertex
        public uint normalVerticesCount;
        public uint alphaVerticesCount;
        public PhyVertex[] verticesBuffer;
        public VertexPositionColorTexture[] verticesOutBuffer;
        public uint normalIndiceCount;
        public uint alphaIndiceCount;
        public ushort[] indiceBuffer;

        public string textureName;
        public int nTexture;
        public int nTexture2;

        public Vector3 boundingBoxMin;
        public Vector3 boundingBoxMax;
        public C3_Moti motion;
        public float fA, fR, fG, fB;
        public C3_Key key;
        public bool bDraw;
        public uint textureRow;
        public Matrix initMatrix;
        public Vector2 uvStep;

        private bool found2sidFlag = false;

        public VertexPositionTexture [] vertexPositionTexture;

        IndexBuffer ibuffer;
        VertexBuffer vBuffer;

        public C3_Phy(BinaryReader reader)
        {
            LoadFile(reader);
            
        }
        public void SetData(GraphicsDevice GraphicsDevice)
        {
            ibuffer = new IndexBuffer(GraphicsDevice, IndexElementSize.SixteenBits, indiceBuffer.Length, BufferUsage.WriteOnly);
            ibuffer.SetData<ushort>(indiceBuffer);
            vBuffer = new VertexBuffer(GraphicsDevice, typeof(VertexPositionColorTexture), verticesOutBuffer.Length, BufferUsage.WriteOnly);
            vBuffer.SetData<VertexPositionColorTexture>(verticesOutBuffer);
        }

        private void LoadFile(BinaryReader reader)
        {
            modelName = ASCIIEncoding.ASCII.GetString(reader.ReadBytes(reader.ReadInt32()));
            Console.WriteLine("PartName: " + modelName);

            blendCount = reader.ReadUInt32();
            normalVerticesCount = reader.ReadUInt32();
            alphaVerticesCount = reader.ReadUInt32();

            Console.WriteLine("Num Verticies(alpha): {0}({1})", normalVerticesCount, alphaVerticesCount);

            verticesBuffer = new PhyVertex[normalVerticesCount + alphaVerticesCount];
            verticesOutBuffer = new VertexPositionColorTexture[normalVerticesCount + alphaVerticesCount];


            for (int i = 0; i < (normalVerticesCount + alphaVerticesCount); i++)
            {
                verticesBuffer[i] = ReadPhyVertex(reader);
                verticesOutBuffer[i].Color = Color.White;
                verticesOutBuffer[i].TextureCoordinate = new Vector2(verticesBuffer[i].u, verticesBuffer[i].v);
                verticesOutBuffer[i].Position = verticesBuffer[i].pos[0];
            }


            normalIndiceCount = reader.ReadUInt32();
            alphaIndiceCount = reader.ReadUInt32();

            Console.WriteLine("Num Triangles(alpha):{0}({1})", normalIndiceCount, alphaIndiceCount);

            indiceBuffer = new ushort[(normalIndiceCount + alphaIndiceCount)*3];

            for (int i = 0; i < indiceBuffer.Length; i++)
                indiceBuffer[i] = reader.ReadUInt16();

            textureName = ASCIIEncoding.ASCII.GetString(reader.ReadBytes(reader.ReadInt32()));
            Console.WriteLine("Texture Name: " + textureName);

            boundingBoxMin = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            boundingBoxMax = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

            initMatrix = new Matrix(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(),
                                            reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(),
                                            reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(),
                                            reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

            textureRow = reader.ReadUInt32();

            //uint frameAlphaCount = reader.ReadUInt32();
            key = new C3_Key();
            key.countAlphas = reader.ReadUInt32();
            key.alphas = new C3_Frame[key.countAlphas];
            for (int i = 0; i < key.countAlphas; i++)
            {
                key.alphas[i].frame = reader.ReadInt32();
                key.alphas[i].fParam = reader.ReadSingle();
                key.alphas[i].bParam = (reader.ReadInt32() > 0 ? true : false);
                key.alphas[i].nParam = reader.ReadInt32();
            }

            key.countDraws = reader.ReadUInt32();
            key.draws = new C3_Frame[key.countDraws];
            for (int i = 0; i < key.countDraws; i++)
            {
                key.draws[i].frame = reader.ReadInt32();
                key.draws[i].fParam = reader.ReadSingle();
                key.draws[i].bParam = (reader.ReadInt32() > 0 ? true : false);
                key.draws[i].nParam = reader.ReadInt32();
            }

            key.countChangeTexs = reader.ReadUInt32();
            key.changeTexs = new C3_Frame[key.countChangeTexs];
            for (int i = 0; i < key.countChangeTexs; i++)
            {
                key.changeTexs[i].frame = reader.ReadInt32();
                key.changeTexs[i].fParam = reader.ReadSingle();
                key.changeTexs[i].bParam = (reader.ReadInt32() > 0 ? true : false);
                key.changeTexs[i].nParam = reader.ReadInt32();
            }

            if (reader.BaseStream.Position + 11 < reader.BaseStream.Length)
                if ((C3PartType)reader.ReadInt32() == C3PartType.STEP)
                {
                    uvStep = new Vector2(reader.ReadSingle(), reader.ReadSingle());
                }
                else
                    reader.BaseStream.Position -= 4;

            if (reader.BaseStream.Position + 3 < reader.BaseStream.Length)
                if ((C3PartType)reader.ReadInt32() == C3PartType._2SID)
                {
                    found2sidFlag = true;
                }
                else
                    reader.BaseStream.Position -= 4;
        }

        public bool Calculate()
        {
            float alpha = 0.0f;
            if (key.ProcessAlpha((uint)motion.nFrame, ref alpha))
                fA = alpha;

            bool draw = false;
            if (key.ProcessDraw((uint)motion.nFrame, ref draw))
                bDraw = draw;

            int tex = -1;
            key.ProcessChangeTexture((uint)motion.nFrame, ref tex);


            if (!bDraw)
                return true;

            Matrix[] bones = new Matrix[motion.bonesCount];

            for(int b = 0; b < motion.bonesCount; b++)
            {
                bones[b] = Matrix.Multiply(initMatrix, motion.GetMatrix(b));
                bones[b] = Matrix.Multiply(bones[b], motion.matrix[b]);
            }

            for(int v = 0; v < (normalVerticesCount + alphaVerticesCount); v++)
            {
                Vector3 mix = verticesBuffer[v].pos[0];

                Vector3 vec;
                verticesOutBuffer[v].Position.X = 0;
                verticesOutBuffer[v].Position.Y = 0;
                verticesOutBuffer[v].Position.Z = 0;

                for(int i = 0; i < 2; i++)
                {
                    uint index = verticesBuffer[v].index[i];
                    float weight = verticesBuffer[v].weight[i];
                    if(weight > 0)
                    {
                        vec = Vector3.Transform(mix, bones[index]);
                        vec = Vector3.Add(verticesOutBuffer[v].Position, vec);
                        break;
                    }
                }

                verticesOutBuffer[v].TextureCoordinate.X += uvStep.X;
                verticesOutBuffer[v].TextureCoordinate.Y += uvStep.Y;

                if(tex > -1)
                {
                    float seqsize = 1.0f / textureRow;
                    verticesOutBuffer[v].TextureCoordinate.X = verticesBuffer[v].u + tex % textureRow * seqsize;
                    verticesOutBuffer[v].TextureCoordinate.Y = verticesBuffer[v].v + tex / textureRow * seqsize;
                }
            }
            return true;
        }
        public void Multiply(int boneIndex, Matrix ij)
        {
            int start = boneIndex;
            int end = start + 1;

            if(boneIndex == -1)
            {
                start = 0;
                end = (int)this.motion.bonesCount;
            }

            for (int n = start; n < end; n++)
                this.motion.matrix[n] = this.motion.matrix[n] * ij;
        }

        public void DrawNormal(GraphicsDevice GraphicsDevice, BasicEffect baseEffect)
        {

            GraphicsDevice.SetVertexBuffer(vBuffer);
            GraphicsDevice.Indices = ibuffer;
            foreach (var pass in baseEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, (int)indiceBuffer.Length);
            }
        }

        public void NextFrame(int step)
        {
            motion.nFrame = (int)((motion.nFrame + step) % motion.framesCount);
        }
    }
}
