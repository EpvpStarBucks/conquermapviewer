﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static COMapViewer.C3.C3File;

namespace COMapViewer.C3
{
    public class C3_Moti
    {
        public uint keyType;
        public uint bonesCount;
        public uint framesCount;
        public uint keyFramesCount;
        public C3KeyFrame[] keyFrames;   // size = keyFramesCount
        public Matrix[] matrix;      // size = boneCount
        public uint morphsCount;
        public float[][] morphs;               // size = morphsCount * framesCount
        public int nFrame;
        public C3_Moti(BinaryReader reader)
        {
            LoadFile(reader);
        }
        private void LoadFile(BinaryReader reader)
        {
            bonesCount = reader.ReadUInt32();
            matrix = new Matrix[bonesCount];
            for (int i =0; i < bonesCount; i++)
                matrix[i] = Matrix.Identity;

            framesCount = reader.ReadUInt32();

            uint keyType = reader.ReadUInt32();

            

            switch ((C3PartType)keyType)
            {
                case C3PartType.XKEY:
                    keyFramesCount = reader.ReadUInt32();
                    keyFrames = new C3KeyFrame[keyFramesCount];
                    for (int frame = 0; frame < keyFramesCount; frame++)
                    {
                        keyFrames[frame].pos = reader.ReadUInt16();
                        keyFrames[frame].matrix = new Matrix[bonesCount];
                        for (int bone = 0; bone < bonesCount; bone++)
                        {
                            keyFrames[frame].matrix[bone].M11 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M12 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M13 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M14 = 0.0f;
                            keyFrames[frame].matrix[bone].M21 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M22 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M23 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M24 = 0.0f;
                            keyFrames[frame].matrix[bone].M31 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M32 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M33 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M34 = 0.0f;
                            keyFrames[frame].matrix[bone].M41 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M42 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M43 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M44 = 0.0f;
                        }
                    }
                    break;
                case C3PartType.KKEY:
                    keyFramesCount = reader.ReadUInt32();
                    keyFrames = new C3KeyFrame[keyFramesCount];
                    for (int frame = 0; frame < keyFramesCount; frame++)
                    {
                        keyFrames[frame].pos = reader.ReadUInt32();
                        keyFrames[frame].matrix = new Matrix[bonesCount];
                        for (int bone = 0; bone < bonesCount; bone++)
                            for (int i = 0; i < 16; i++)
                                keyFrames[frame].matrix[bone][i] = reader.ReadSingle();
                    }
                    break;
                case C3PartType.ZKEY:
                    keyFramesCount = reader.ReadUInt32();
                    keyFrames = new C3KeyFrame[keyFramesCount];
                    Quaternion tempQ;
                    for (int frame = 0; frame < keyFramesCount; frame++)
                    {
                        keyFrames[frame].pos = reader.ReadUInt16();
                        keyFrames[frame].matrix = new Matrix[bonesCount];
                        for (int bone = 0; bone < bonesCount; bone++)
                        {
                            tempQ = new Quaternion(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

                            /* TODO: Determine if this is the correct way to handle quaternion */

                            keyFrames[frame].matrix[bone] = Matrix.Identity * Matrix.CreateFromQuaternion(tempQ);
                            keyFrames[frame].matrix[bone].M41 = tempQ.X;
                            keyFrames[frame].matrix[bone].M42 = tempQ.Y;
                            keyFrames[frame].matrix[bone].M43 = tempQ.Z;
                            keyFrames[frame].matrix[bone].M41 = 1.0f;

                        }
                    }
                    break;
                default:
                    reader.BaseStream.Position -= 4;
                    keyFramesCount = framesCount;
                    keyFrames = new C3KeyFrame[keyFramesCount];
                    for(int kk = 0; kk < framesCount; kk++)
                    {
                        keyFrames[kk].pos = (uint)kk;
                        keyFrames[kk].matrix = new Matrix[bonesCount];
                    }
                    for (int frame = 0; frame < keyFramesCount; frame++)
                    {
                        for (int bone = 0; bone < bonesCount; bone++)
                        {
                            keyFrames[frame].matrix[bone].M11 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M12 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M13 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M14 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M21 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M22 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M23 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M24 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M31 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M32 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M33 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M34 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M41 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M42 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M43 = reader.ReadSingle();
                            keyFrames[frame].matrix[bone].M44 = reader.ReadSingle();
                        }
                    }
                    break;
            }

            morphsCount = reader.ReadUInt32();
            morphs = new float[morphsCount][];
            for (int m = 0; m < morphsCount; m++)
            {
                morphs[m] = new float[framesCount];

                for (int f = 0; f < framesCount; f++)
                    morphs[m][f] = reader.ReadSingle();
            }

        }

        public Matrix GetMatrix(int bone)
        {
            int sIndex = -1, eIndex = -1;
            for (int n = 0; n < this.keyFramesCount; n++)
            {
                if (keyFrames[n].pos <= nFrame)
                    if (sIndex == -1 || n > sIndex)
                        sIndex = n;
                if (keyFrames[n].pos > nFrame)
                    if (eIndex == -1 || n < eIndex)
                        eIndex = n;
            }
            if (sIndex == -1 && eIndex > 1)
                return keyFrames[eIndex].matrix[bone];
            else if (sIndex > -1 && eIndex == -1)
                return keyFrames[sIndex].matrix[bone];
            else if (sIndex > -1 && eIndex > -1)
                return Matrix.Add(keyFrames[sIndex].matrix[bone], Matrix.Multiply((keyFrames[eIndex].matrix[bone] - keyFrames[sIndex].matrix[bone]), ((float)nFrame - keyFrames[sIndex].pos) / ((float)(keyFrames[eIndex].pos - keyFrames[sIndex].pos))));
            return Matrix.Identity;
        }
    }
}
