﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace COMapViewer.DMap
{
    public enum MapLayerType
    {
        NONE = 0,
        TERRAIN = 1,
        FLOOR = 2,
        INTERACTIVE = 3,
        SCENE = 4,
        SKY = 5,
        SURFACE = 6
    }
    public abstract class MapLayer
    {

        private int m_MoveRateX;
        private int m_MoveRateY;
        public MapLayer()
        {
            m_MoveRateX = 100;
            m_MoveRateY = 100;
        }

        public void SetMoveRateX(int MoveRateX) { m_MoveRateX = MoveRateX; }
        public void SetMoveRateY(int MoveRateY) { m_MoveRateY = MoveRateY; }
        public int GetMoveRateX() { return m_MoveRateX; }
        public int GetMoveRateY() { return m_MoveRateY; }

        public virtual MapLayerType GetMapLayerType() { return MapLayerType.NONE; }
        public virtual void Draw(SpriteBatch spriteBatch, Rectangle drawWindow) { throw new NotImplementedException(); }
        public virtual void Process(Vector2 posUpdate) {  }
        public virtual void LoadData(BinaryReader reader) { throw new NotImplementedException(); }

    }
}
