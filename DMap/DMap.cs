﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using COMapViewer.Files;
using MonoGame.Extended.Shapes;
using MonoGame.Extended;

namespace COMapViewer.DMap
{


    enum MapObjectType
    {
        Scene = 0x01,
        TerrainObject = 0x04,
        Backdrop = 0x08,
        Effect = 0x0A,
        Sound = 0x0F
    }
    public class Scene
    {
        public string filePath { get; set; }
        public Point location { get; set; }

        public Scene(BinaryReader reader)
        {
            filePath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(260));
            location = new Point(reader.ReadInt32(), reader.ReadInt32());
        }
        public void Write(BinaryWriter writer)
        {
            writer.Write((int)MapObjectType.Scene);
            writer.Write(ASCIIEncoding.ASCII.GetBytes(filePath.PadRight(260, '\0')));
            writer.Write(location.X);
            writer.Write(location.Y);
        }
           
    }
    public class TerrainObject
    {
        /// <summary>
        /// selected is used to mask the object select in winform
        /// </summary>
        public bool selected;
        public string aniPath { get; set; }
        public string aniName { get; set; }
        /// <summary>
        /// location is cell location
        /// </summary>
        public Point location { get; set; }
        /// <summary>
        /// size is in number of tiles??
        /// </summary>
        public Size size { get; set; }
        /// <summary>
        /// offset is pixel offset from cell world pixel.
        /// </summary>
        public Point offset { get; set; }
        public int interval { get; set; }

        public TerrainObject(BinaryReader reader)
        {
            aniPath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(260));
            aniName = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(128));
            location = new Point(reader.ReadInt32(), reader.ReadInt32());
            size = new Size(reader.ReadInt32(), reader.ReadInt32());
            offset = new Point(reader.ReadInt32(), reader.ReadInt32());
            interval = reader.ReadInt32();
        }
        public void Write(BinaryWriter writer)
        {
            writer.Write((int)MapObjectType.TerrainObject);
            writer.Write(ASCIIEncoding.ASCII.GetBytes(aniPath.PadRight(260, '\0')));
            writer.Write(ASCIIEncoding.ASCII.GetBytes(aniName.PadRight(128, '\0')));
            writer.Write(location.X);
            writer.Write(location.Y);
            writer.Write(size.Width);
            writer.Write(size.Height);
            writer.Write(offset.X);
            writer.Write(offset.Y);
            writer.Write(interval);
        }
        public void Draw(SpriteBatch spriteBatch, Rectangle drawWindow, DataMap mapObj)
        {
            Point worldPos = mapObj.Cell2Bg(location);
            Rectangle terrainRec = new Rectangle(new Point(worldPos.X - offset.X , worldPos.Y- offset.Y), AniCollectionManager.GetTexture2DSize(aniPath, aniName, 0));
            Color drawcolor = Color.White;
            if (selected)
                drawcolor = new Color(Color.Red, 0.6f);
            if(drawWindow.Intersects(terrainRec))
            {
                Texture2D tex = AniCollectionManager.GetTexture2D(aniPath, aniName, 0);
                if (tex != null)
                    spriteBatch.Draw(tex, new Vector2(terrainRec.X - drawWindow.X, terrainRec.Y - drawWindow.Y), drawcolor);
            }


            //Console.WriteLine("Drawing Puzzle: {0} Count: {1}", posShow, drawcount);
        }

    }
    public class Sound
    {
        public string soundPath { get; set; }
        public Point location { get; set; }
        public int volume { get; set; }
        public int range { get; set; }

        public Sound(BinaryReader reader)
        {
            soundPath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(260));
            location = new Point(reader.ReadInt32(), reader.ReadInt32());
            volume = reader.ReadInt32();
            range = reader.ReadInt32();
        }
        public void Write(BinaryWriter writer)
        {
            writer.Write((int)MapObjectType.Sound);
            writer.Write(ASCIIEncoding.ASCII.GetBytes(soundPath.PadRight(260, '\0')));
            writer.Write(location.X);
            writer.Write(location.Y);
            writer.Write(volume);
            writer.Write(range);
        }
    }
    public class Effect
    {
        public string effectName { get; set; }
        public Point location { get; set; }
        public Effect(BinaryReader reader)
        {
            effectName = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(64));
            location = new Point(reader.ReadInt32(), reader.ReadInt32());
        }
        public void Write(BinaryWriter writer)
        {
            writer.Write((int)MapObjectType.Effect);
            writer.Write(ASCIIEncoding.ASCII.GetBytes(effectName.PadRight(64, '\0')));
            writer.Write(location.X);
            writer.Write(location.Y);
        }
    }
    public class Backdrop
    {
        public string filePath { get; set; }

        public Backdrop(BinaryReader reader)
        {
            filePath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(260));
        }
        public void Write(BinaryWriter writer)
        {
            writer.Write((int)MapObjectType.Backdrop);
            writer.Write(ASCIIEncoding.ASCII.GetBytes(filePath.PadRight(260, '\0')));
        }
    }
    public class DMapLayer
    {
        public List<Backdrop> backdrops { get; set; }
        public List<TerrainObject> terrainObjects { get; set; }
        public int layerType { get; set; }
        public int xInterval { get; set; }
        public int yInterval { get; set; }
        public int index { get; set; }

        public void Write(BinaryWriter writer)
        {
            writer.Write(index);
            writer.Write(layerType);
            writer.Write(xInterval);
            writer.Write(yInterval);
            int objCount = backdrops.Count + terrainObjects.Count;
            
            writer.Write(objCount);
            for(int cnt = 0; cnt < backdrops.Count; cnt++)
            {
                backdrops[cnt].Write(writer);
            }

            for (int cnt = 0; cnt < terrainObjects.Count; cnt++)
            {
                terrainObjects[cnt].Write(writer);
            }
        }

    }
    public class Portal
    {
        public Point location { get;  set; }
        public int portalID { get; set; }
        public Portal(BinaryReader reader)
        {
            location = new Point(reader.ReadInt32(), reader.ReadInt32());
            portalID = reader.ReadInt32();
        }
        public void Write(BinaryWriter writer)
        {
            writer.Write(location.X);
            writer.Write(location.Y);
            writer.Write(portalID);
        }
    }
    public class DataMap
    {
        internal enum MapSelectionMode
        {
            General,
            Accessable,
            Inaccessable
        }
        private MapSelectionMode mapSelectionMode = MapSelectionMode.General;

        public bool DrawTerrainObjects { get; set; }
        public bool DrawGrid { get; set; }

        public List<TerrainObject> terrainObjects { get; set; }
        public List<Scene> scenes { get; set; }
        public List<Sound> sounds { get; set; }
        public List<Effect> effects { get; set; }
        public List<DMapLayer> dMapLayers { get; set; }
        public List<Portal> portals { get; set; }
        public Cell [,] cells { get; set; }

        public string puzzlePath { get; set; }
        private PuzzleFile puzzleContent;

        public Size mapSize { get; set; }
        public Size mapWorldSize { get { return mapSize * puzzleContent.PUZZLESIZE.Width;  } }
        public string dMapHeader { get; set; }

        public bool SaveDataMap(BinaryWriter writer)
        {
            try
            {
                writer.Write(ASCIIEncoding.ASCII.GetBytes(dMapHeader.PadRight(8, '\0')));
                writer.Write(ASCIIEncoding.ASCII.GetBytes(puzzlePath.PadRight(260, '\0')));
                writer.Write(mapSize.Width);
                writer.Write(mapSize.Height);
                for (int i = 0; i < mapSize.Height; i++)
                {
                    ulong checksum = 0;
                    for (int j = 0; j < mapSize.Width; j++)
                    {
                        cells[j, i].Write(writer);
                        checksum += (ulong)(cells[j, i].access * (cells[j, i].surface + i + 1) + (cells[j, i].elevation + 2) * (j + 1 + cells[j, i].surface));
                    }
                    writer.Write((uint)checksum);
                }
                writer.Write(portals.Count);
                for (int i = 0; i < portals.Count; i++)
                {
                    portals[i].Write(writer);
                }
                int objCount = scenes.Count + terrainObjects.Count + effects.Count + sounds.Count;
                writer.Write(objCount);
                for (int cnt = 0; cnt < scenes.Count; cnt++)
                    scenes[cnt].Write(writer);
                for (int cnt = 0; cnt < terrainObjects.Count; cnt++)
                    terrainObjects[cnt].Write(writer);
                for (int cnt = 0; cnt < effects.Count; cnt++)
                    effects[cnt].Write(writer);
                for (int cnt = 0; cnt < sounds.Count; cnt++)
                    sounds[cnt].Write(writer);

                writer.Write(dMapLayers.Count);
                for (int cnt = 0; cnt < dMapLayers.Count; cnt++)
                    dMapLayers[cnt].Write(writer);
            }
            catch(Exception e) { Debug.WriteLine("[DMap][SaveDataMap] Error saving map {0}", e.Message); return false; }
            return true;

        }
        public static  DataMap LoadDataMap(string fileName)
        {
            DataMap dMap = new DataMap()
                {
                    terrainObjects = new List<TerrainObject>(),
                    scenes = new List<Scene>(),
                    sounds = new List<Sound>(),
                    effects = new List<Effect>()
                };
            using (BinaryReader reader = new BinaryReader(File.OpenRead(fileName)))
            {
                dMap.dMapHeader = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(8));
                dMap.puzzlePath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(260));
                dMap.puzzleContent = new PuzzleFile(dMap.puzzlePath);

                dMap.mapSize = new Size(reader.ReadInt32(), reader.ReadInt32());

                dMap.cells = new Cell[dMap.mapSize.Width, dMap.mapSize.Height];

                for (int i = 0; i < dMap.mapSize.Height; i++)
                {
                    ulong dwCheckData = 0;
                    for (int j = 0; j < dMap.mapSize.Width; j++)
                    {
                        dMap.cells[j, i] = new Cell(reader);
                        dwCheckData += (ulong)(dMap.cells[j, i].access * (dMap.cells[j, i].surface + i + 1) + (dMap.cells[j, i].elevation + 2) * (j + 1 + dMap.cells[j, i].surface));
                    }
                    uint dwMapCheckData = reader.ReadUInt32();
                    if (dwMapCheckData != dwCheckData)
                        Debug.WriteLine("[Dmap] [LoadDataMap] Checksum doesn't match");
                }
                int portalCount = reader.ReadInt32();
                dMap.portals = new List<Portal>(portalCount);
                for (int i = 0; i < portalCount; i++)
                {
                    dMap.portals.Add(new Portal(reader));
                }

                int objCount = reader.ReadInt32();
                for (int i = 0; i < objCount; i++)
                {
                    MapObjectType objType = (MapObjectType)reader.ReadUInt32();

                    switch (objType)
                    {
                        case MapObjectType.Scene:
                            dMap.scenes.Add(new Scene(reader));
                            break;
                        case MapObjectType.TerrainObject:
                            TerrainObject tObj = new TerrainObject(reader);
                            dMap.terrainObjects.Add(tObj);
                            break;
                        case MapObjectType.Effect:
                            dMap.effects.Add(new Effect(reader));
                            break;
                        case MapObjectType.Sound:
                            dMap.sounds.Add(new Sound(reader));
                            break;
                        default:
                            Debug.WriteLine("[DMap] [LoadDataMap] Unsupported Layer Type:{0}", objType);
                            break;
                    }
                }

                int layCount = reader.ReadInt32();
                dMap.dMapLayers = new List<DMapLayer>(layCount);
                for (int i = 0; i < layCount; i++)
                {
                    DMapLayer lay = new DMapLayer()
                    {
                        backdrops = new List<Backdrop>(),
                        terrainObjects = new List<TerrainObject>(),
                        index = reader.ReadInt32(),
                        layerType = reader.ReadInt32(),
                        xInterval = reader.ReadInt32(),
                        yInterval = reader.ReadInt32()
                    };

                    int objCnt = reader.ReadInt32();
                    for (int j = 0; j < objCnt; j++)
                    {
                        MapObjectType type = (MapObjectType)reader.ReadInt32();
                        switch (type)
                        {
                            case MapObjectType.TerrainObject:
                                lay.terrainObjects.Add(new TerrainObject(reader));
                                break;
                            case MapObjectType.Backdrop:
                                lay.backdrops.Add(new Backdrop(reader));
                                break;
                            default:
                                Debug.WriteLine("[DMap] [LoadDataMap] Unsupported Scene Type:{0}", type);
                                break;
                        }
                    }
                    dMap.dMapLayers.Add(lay);
                }

                if (reader.BaseStream.Position != reader.BaseStream.Length)
                    Debug.WriteLine("[Dmap] [LoadDataMap] Missing Map data {0}/{1}", reader.BaseStream.Position, reader.BaseStream.Length);
            }
            return dMap;
        }

        public void Draw(SpriteBatch spriteBatch, Rectangle drawWindow, Point mousePosition)
        {
            puzzleContent.Draw(spriteBatch, drawWindow);
            if(DrawTerrainObjects)
                foreach (TerrainObject tobj in terrainObjects)
                    tobj.Draw(spriteBatch, drawWindow, this);

            if(DrawGrid)
            {
                Polygon p = new Polygon(new Vector2[4] { new Vector2(32, 0), new Vector2(0, 16), new Vector2(32, 32), new Vector2(64, 16) });

                int numCellsWidth = drawWindow.Width / 64 + 2;
                int numCellHeight = drawWindow.Height / 32 + 2;

                int xOffset = drawWindow.X % 64 + 32;
                int yOffset = drawWindow.Y % 32;

                int drawX = -xOffset;
                int drawY = -yOffset;

                for(int x = 0; x < numCellsWidth*2; x++)
                {
                    for(int y = 0; y < numCellHeight; y++)
                    {
                        Rectangle rec = new Rectangle(new Point(x * 32 + drawX, y * 32 + (drawY - (x % 2) * 16)), new Point(64, 32));

                        Point cell = Bg2Cell(rec.Location + drawWindow.Location + new Point(32,16));
                        if (GetCellAccess(cell) == 0)
                        {
                            spriteBatch.Draw(GlobalContent.accessableTileTransparent, new Vector2(x * 32 + drawX, y * 32 + (drawY - (x % 2) * 16)), Color.White);
                        }
                        else
                            spriteBatch.Draw(GlobalContent.inaccessableTileTransparent, new Vector2(x * 32 + drawX, y * 32 + (drawY - (x % 2) * 16)), Color.White);
                        
                        if(p.Contains(mousePosition.ToVector2() - rec.Location.ToVector2()))
                        {
                            switch(mapSelectionMode)
                            {
                                case MapSelectionMode.General:
                                    spriteBatch.Draw(GlobalContent.selectTile, new Vector2(x * 32 + drawX, y * 32 + (drawY - (x % 2) * 16)));
                                    break;
                                case MapSelectionMode.Accessable:
                                    spriteBatch.Draw(GlobalContent.selectTileAccessable, new Vector2(x * 32 + drawX, y * 32 + (drawY - (x % 2) * 16)));
                                    SetCellAccess(Bg2Cell(mousePosition + drawWindow.Location),0);
                                    break;
                                case MapSelectionMode.Inaccessable:
                                    spriteBatch.Draw(GlobalContent.selectTileInaccessable, new Vector2(x * 32 + drawX, y * 32 + (drawY - (x % 2) * 16)));
                                    SetCellAccess(Bg2Cell(mousePosition + drawWindow.Location), 1);
                                    break;
                            }
                        }
                        //spriteBatch.DrawPolygon(new Vector2(x*32 + drawX, y * 32 + (drawY - ( x % 2 )* 16 ) ), p, color);
                    }
                }
            }
        }

        public void MouseClick(string button, Rectangle drawWindow, Point mousePosition)
        {
            switch(button)
            {
                case "leftdown":
                    mapSelectionMode = MapSelectionMode.Accessable;
                    break;
                case "leftup":
                    mapSelectionMode = MapSelectionMode.General;
                    break;
                case "rightdown":
                    mapSelectionMode = MapSelectionMode.Inaccessable;
                    break;
                case "rightup":
                    mapSelectionMode = MapSelectionMode.General;
                    break;
                case "middle":
                    break;
            }
        }

        public Point Cell2World(Point cell)
        {
            Point world = new Point(0,0);
            Point mposOrigin = new Point(64 * (mapSize.Width / 2), 32 / 2);

            world.X = 32 * (cell.X - cell.Y) + mposOrigin.X;
            world.Y = 16 * (cell.X + cell.Y) + mposOrigin.Y;
            return world;
        }
        public Point World2Cell( Point posWorld)
        {
            Point cell = new Point(0, 0);
            Point mposOrigin = new Point(64 * (mapSize.Width / 2), 32 / 2);

            double dwx = posWorld.X - mposOrigin.X;
            double dwy = posWorld.Y - mposOrigin.Y;
            double dch = 32.0;
            double dcw = 64.0;

            double dX = (dwx / dcw) + (dwy / dch);
            double dY = (double)(dwy / dch) - (double)(dwx / dcw);

            
            cell.X = (int)System.Math.Round(dX);

            cell.Y = (int)System.Math.Round(dY);

            return cell;
        }
        public Point Bg2Cell(Point bgPos)
        {
            return World2Cell(Bg2World(bgPos));
        }
        public Point Cell2Bg(Point cell)
        {
            return World2Bg(Cell2World(cell));
        }
        public Point Map2Cell(Point mapPos)
        {
            Point retCell;

            retCell.X = mapPos.X * 64;
            retCell.Y = mapPos.Y * 32;

            Size bgSize = GetBgSize();
            retCell.Y = bgSize.Height - retCell.Y;
            return World2Cell(Bg2World(retCell));
        }
        public Point Cell2Map(Point cell)
        {
            Point worldPos = Cell2World(cell);
            Point bg = World2Bg(worldPos);
            Size bgSize = GetBgSize();

            Point mapPos;
            mapPos.X = bg.X;
            mapPos.Y = bgSize.Height - bg.Y;
            //mapPos.X /= 32;
            //mapPos.Y /= 64;

            return mapPos;
        }
        public Point Bg2World(Point posBg)
        {
            Point bgWorldPos = GetBgWorldPos();
            Point worldPos;
            worldPos.X = posBg.X + bgWorldPos.X;
            worldPos.Y = posBg.Y + bgWorldPos.Y;
            return worldPos;
        }
        public Point World2Bg(Point posWorld)
        {
            Point bgPos;
            Point bgWorldPos = GetBgWorldPos();
            bgPos.X = posWorld.X - bgWorldPos.X;
            bgPos.Y = posWorld.Y - bgWorldPos.Y;

            return bgPos;
        }
        /// <summary>
        /// What is BG size??
        /// </summary>
        /// <returns></returns>
        public Size GetBgSize()
        {
            Size bgSize;
            bgSize.Width = puzzleContent.m_infoSize.Width * (puzzleContent.PUZZLESIZE.Width / 1);
            bgSize.Height = puzzleContent.m_infoSize.Height * (puzzleContent.PUZZLESIZE.Width / 1);
            return bgSize;
        }
        public Point GetBgWorldPos()
        {
            Size sizePuzz = GetBgSize();
            Point mposOrigin = new Point(64 * (mapSize.Width / 2), 32 / 2);
            Point bgWorld;

            bgWorld.X = mposOrigin.X - sizePuzz.Width / 2;
            bgWorld.Y = mposOrigin.Y + 32 * mapSize.Height / 2 - sizePuzz.Height / 2;
            bgWorld.Y -= ((mapSize.Height + 1) % 2) * 16;
            return bgWorld;
        }

        public short GetCellAccess(Point cell)
        {
            return GetCellAccess(cell.X, cell.Y);
        }
        public short GetCellAccess(int x, int y)
        {
            if (x >= 0 && x < cells.GetLength(0))
                if (y >= 0 && y < cells.GetLength(1))
                    return cells[x, y].access;
            return short.MaxValue;
        }
        public void SetCellAccess(Point cell, short value)
        {
            SetCellAccess(cell.X, cell.Y, value);
        }
        public void SetCellAccess(int x, int y, short value)
        {
            if (x >= 0 && x < cells.GetLength(0))
                if (y >= 0 && y < cells.GetLength(1))
                    cells[x, y].access = value;
                else
                    Debug.WriteLine("SetCellAccess out of bounds {0},{1}", x, y);
            else
                Debug.WriteLine("SetCellAccess out of bounds {0},{1}", x, y);
        }
        public short GetCellSurface(int x, int y)
        {
            if (x >= 0 && x < cells.GetLength(0))
                if (y >= 0 && y < cells.GetLength(1))
                    return cells[x, y].surface;
            return short.MaxValue;
        }
        public short GetCellElevation(int x, int y)
        {
            if (x >= 0 && x < cells.GetLength(0))
                if (y >= 0 && y < cells.GetLength(1))
                    return cells[x, y].elevation;
            return short.MaxValue;
        }
    }
    public enum LayerType
    {
        NONE = 0,
        TERRAIN = 1,
        TERRAIN_PART = 2,
        SCENE = 3,
        COVER = 4,
        ROLE = 5,
        HERO = 6,
        PLAYER = 7,
        PUZZLE = 8,
        ANIMATION_SIMPLE = 9,
        ANIMATION = 10,
        ITEM = 11,
        NPC = 12,
        OBJECT3D = 13,
        TRACE3D = 14,
        SOUND = 15,
        REGION = 16,
        MAGICMAPITEM = 17,
        MAPITEM = 18,
        EFFECT3D = 19
    }
    
    public class Cell
    {
        public short access { get; set; }
        public short surface { get; set; }
        public short elevation { get; set; }
        public uint portalIndex { get; set; }
        public Cell() { }
        public Cell(BinaryReader reader)
        {
            access = reader.ReadInt16();
            surface = reader.ReadInt16();
            elevation = reader.ReadInt16();
        }
        public void Write(BinaryWriter writer)
        {
            writer.Write(access);
            writer.Write(surface);
            writer.Write(elevation);
        }
    }

}
