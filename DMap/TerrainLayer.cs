﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using COMapViewer.Files;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace COMapViewer.DMap
{

    public class TerrainLayer : MapLayer
    {
        private List<Portal> mPortals;
        private PuzzleFile mPuzzle;

        public TerrainLayer()
        {
            mPortals = new List<Portal>();
        }
        public void LoadPortal(BinaryReader reader)
        {
            int ibound = reader.ReadInt32();
            for (int i = 0; i < ibound; i++)
            {
                mPortals.Add(new Portal(reader));
            }
        }

        public void LoadPuzzle(string puzzleFile)
        {
            mPuzzle = new PuzzleFile(puzzleFile);
        }

        public override MapLayerType GetMapLayerType()
        {
            return MapLayerType.TERRAIN;
        }
        public override void Draw(SpriteBatch spriteBatch, Rectangle drawWindow)
        {
            mPuzzle.Draw(spriteBatch, drawWindow);
            //Draw portals
        }
        public override void Process(Vector2 posUpdate)
        {
            base.Process(posUpdate);
        }
    }
}
