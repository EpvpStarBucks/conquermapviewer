﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace COMapViewer.DMap
{
    public class InteractiveLayer : MapLayer
    {
        private List<MapObject> setMapObjects;

        public InteractiveLayer()
        {
            setMapObjects = new List<MapObject>();
        }
        public override MapLayerType GetMapLayerType()
        {
            return MapLayerType.INTERACTIVE;
        }
        public override void LoadData(BinaryReader reader)
        {
            if (reader == null)
                return;
            uint LayerAmount = reader.ReadUInt32();
            for (int i = 0; i < LayerAmount; i++)
            {
                uint layerType = reader.ReadUInt32();
                string fileNametoShow = "Fail";
                switch (layerType)
                {
                    case 1: //TERRAIN
                        string tfilePath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(260));
                        uint toriginX = reader.ReadUInt32();
                        uint toriginY = reader.ReadUInt32();
                        //SceneFile scene = new SceneFile(reader);
                        fileNametoShow = "Terrian: " + tfilePath;
                        break;
                    case 4: // COVER
                        string cfilePath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(260));
                        string cKeyName = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(128));
                        uint coriginX = reader.ReadUInt32();
                        uint coriginY = reader.ReadUInt32();
                        uint cwidth = reader.ReadUInt32();
                        uint cheight = reader.ReadUInt32();
                        uint coffsetx = reader.ReadUInt32();
                        uint coffsety = reader.ReadUInt32();
                        uint cframeInterval = reader.ReadUInt32();
                        fileNametoShow = "Cover: " + cfilePath + " KeyName: " + cKeyName;
                        break;
                    case 10: //3D EFFECT
                        string defilePath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(64));
                        uint deoriginX = reader.ReadUInt32();
                        uint deoriginY = reader.ReadUInt32();
                        fileNametoShow = "3d Effect: " + defilePath;
                        break;
                    case 15: //SOUND
                        string sofilePath = BaseFunc.ReadCString(ASCIIEncoding.ASCII, reader.ReadBytes(260));
                        uint sooriginX = reader.ReadUInt32();
                        uint sooriginY = reader.ReadUInt32();
                        uint sorange = reader.ReadUInt32();
                        uint soVolume = reader.ReadUInt32();
                        fileNametoShow = "Sound: " + sofilePath;
                        break;
                    default:
                        Debug.WriteLine("[DMap] [LoadDataMap] Unsupported Layer Type:{0}", layerType);
                        break;
                }
                //if (Game1.DEBUGMODE)
                //    Debug.WriteLine("[{0}/{2}] {1}", i + 1, fileNametoShow, LayerAmount);
            }
        }
    }
}
