﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoGame.Extended.Shapes;
using COMapViewer.C3;
using static COMapViewer.C3.C3File;


namespace COMapViewer
{
    public static class GlobalContent
    {
        public static Texture2D sheet;
        public static Texture2D sheetTransparent;
        public static Texture2D accessableTile;
        public static Texture2D accessableTileTransparent;
        public static Texture2D inaccessableTile;
        public static Texture2D inaccessableTileTransparent;
        public static Texture2D sheetSelect;
        public static Texture2D selectTile;
        public static Texture2D selectTileAccessable;
        public static Texture2D selectTileInaccessable;
    }
    public class Camera2d
    {
        protected float _zoom; // Camera Zoom
        public Matrix _transform; // Matrix Transform
        public Vector2 _pos; // Camera Position
        protected float _rotation; // Camera Rotation

        public Camera2d()
        {
            _zoom = 1f;
            _rotation = 0.0f;
            _pos = new Vector2(0, 0);
        }
        public float Zoom
        {
            get { return _zoom; }
            set { _zoom = value; if (_zoom < 0.01f) _zoom = 0.01f; } // Negative zoom will flip image
        }

        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        // Auxiliary function to move the camera
        public void Move(Vector2 amount)
        {
            _pos += amount;
        }
        // Get set position
        public Vector2 Pos
        {
            get { return _pos; }
            set { _pos = value; }
        }
        public Matrix get_transformation(GraphicsDevice graphicsDevice)
        {
            _transform =       // Thanks to o KB o for this solution
              Matrix.CreateTranslation(new Vector3(-_pos.X, -_pos.Y, 0)) *
                                         Matrix.CreateRotationZ(Rotation) *
                                         Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
                                         Matrix.CreateTranslation(new Vector3(-graphicsDevice.Viewport.Width * 0.5f, -graphicsDevice.Viewport.Height * 0.5f, 0));
            return _transform;
        }
    }
    public class Game1 : Game
    {
        public MapDataView myForm;
        public static bool DEBUGMODE = false;
        public static string BaseClientPath = @"D:\Programming\Conquer\Clients\5095\";

        GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private SpriteBatch overlay;
        private SpriteBatch gridOverlay;



        private readonly List<string> files;
        private string imgFile = "";
        private int imgIndex;
        private MouseState oldMState;
        private KeyboardState oldKBState;
        private SpriteFont defaultFont;

        private DMap.DataMap gameDataMap;
        private Camera2d cam;
        private Rectangle drawWindow;
        private Vector2 effectiveMousePos;
        private int previousScrollValue;

        bool drawGrid = false;

        C3_Phy myPhy;
        VertexPositionTexture[] vertBuff;
        IndexBuffer ibuffer;
        VertexBuffer vBuffer;
        BasicEffect baseEffect;

        Matrix viewMatrix;
        Matrix projectionMatrix;

        VertexPositionColor[] lines;

        float positionX = 30;
        float positionY = 10;
        float positionZ = 30;
        Texture2D mytext;
        private void NextFile()
        {
            imgIndex++;
            if (imgIndex >= files.Count)
            {
                imgIndex = 0;
            }

            imgFile = files[imgIndex];
        }

        private void PreviousFile()
        {
            imgIndex--;
            if (imgIndex <= 0)
            {
                imgIndex = files.Count - 1;
            }

            imgFile = files[imgIndex];
        }

        private void LoadMap()
        {
            //Content.Unload();
            gameDataMap = DMap.DataMap.LoadDataMap(files[imgIndex]);
            myForm.SetGameMap(gameDataMap);

            if (imgFile != "")
            {
                Window.Title = Path.GetFileName(imgFile);
            }
        }
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1920,
                PreferredBackBufferHeight = 1080
                
            };

            Content.RootDirectory = "Content";

            files = Directory.EnumerateFiles(BaseClientPath + @"\map\map", "*.DMap").ToList();
            imgIndex = 1;

            IsMouseVisible = true;

            cam = new Camera2d();

            //LoadMap();
            myForm = new MapDataView();

        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            AniCollectionManager.SetGraphicsDevice(GraphicsDevice);

            previousScrollValue = Mouse.GetState().ScrollWheelValue;


            gameDataMap = DMap.DataMap.LoadDataMap(BaseClientPath + @"\map\map\faction.dmap");
            myForm.SetGameMap(gameDataMap);
            /* 3d Modeling Experiment
            viewMatrix = Matrix.CreateLookAt(new Vector3(50, 0, 0), new Vector3(0, 0, 0), new Vector3(0, 1, 0));

            viewMatrix = Matrix.CreateLookAt(new Vector3(positionX, positionY, positionZ), new Vector3(0, 0, 0), new Vector3(0, 1, 0));

            //projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, GraphicsDevice.Viewport.AspectRatio, 0.1f, 500);
            projectionMatrix = Matrix.CreateOrthographic(300, 300, 0.1f, 300);
            */
        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            overlay = new SpriteBatch(GraphicsDevice);
            gridOverlay = new SpriteBatch(GraphicsDevice);

            GlobalContent.sheet = Content.Load<Texture2D>("tiles/tiles");
            Color[] tempData = new Color[64 * 32];

            GlobalContent.inaccessableTile = new Texture2D(GraphicsDevice, 64, 32);
            GlobalContent.sheet.GetData<Color>(0, new Rectangle(0, 0, 64, 32), tempData, 0, tempData.Length);
            GlobalContent.inaccessableTile.SetData<Color>(tempData);

            GlobalContent.accessableTile = new Texture2D(GraphicsDevice, 64, 32);
            GlobalContent.sheet.GetData<Color>(0, new Rectangle(0, 32, 64, 32), tempData, 0, tempData.Length);
            GlobalContent.accessableTile.SetData<Color>(tempData);


            GlobalContent.sheetTransparent = Content.Load<Texture2D>("tiles/tiles_transparent");

            GlobalContent.inaccessableTileTransparent = new Texture2D(GraphicsDevice, 64, 32);
            GlobalContent.sheetTransparent.GetData<Color>(0, new Rectangle(0, 0, 64, 32), tempData, 0, tempData.Length);
            GlobalContent.inaccessableTileTransparent.SetData<Color>(tempData);

            GlobalContent.accessableTileTransparent = new Texture2D(GraphicsDevice, 64, 32);
            GlobalContent.sheetTransparent.GetData<Color>(0, new Rectangle(0, 32, 64, 32), tempData, 0, tempData.Length);
            GlobalContent.accessableTileTransparent.SetData<Color>(tempData);

            GlobalContent.sheetSelect = Content.Load<Texture2D>("tiles/tiles_selection");

            GlobalContent.selectTileInaccessable = new Texture2D(GraphicsDevice, 64, 32);
            GlobalContent.sheetSelect.GetData<Color>(0, new Rectangle(0, 0, 64, 32), tempData, 0, tempData.Length);
            GlobalContent.selectTileInaccessable.SetData<Color>(tempData);

            GlobalContent.selectTileAccessable = new Texture2D(GraphicsDevice, 64, 32);
            GlobalContent.sheetSelect.GetData<Color>(0, new Rectangle(0, 32, 64, 32), tempData, 0, tempData.Length);
            GlobalContent.selectTileAccessable.SetData<Color>(tempData);

            GlobalContent.selectTile = new Texture2D(GraphicsDevice, 64, 32);
            GlobalContent.sheetSelect.GetData<Color>(0, new Rectangle(0, 64, 64, 32), tempData, 0, tempData.Length);
            GlobalContent.selectTile.SetData<Color>(tempData);


            defaultFont = Content.Load<SpriteFont>("fonts/default");
            // Load image data into memory


            #region 3D Stuff
            /* 3D Modeling Experiment

            //myPhy = C3File.LoadFile(@"C:\Temp\c3\mesh\480280.c3");
            //mytext = Content.Load<Texture2D>("c3/texture/480285");
            //myPhy = C3File.LoadFile(@"C:\Temp\c3\npc\999001101.c3");
            //mytext = Content.Load<Texture2D>("c3/texture/9990211");
            myPhy = C3File.LoadFile(@"C:\Temp\c3\monster\160\1.c3");
            //foreach (VertexPositionColorTexture v in myPhy.verticesOutBuffer)
            //    Console.WriteLine("Vertex: {0}", v.Position);
            myPhy.SetData(GraphicsDevice);

            baseEffect = new BasicEffect(GraphicsDevice);

            baseEffect.Texture = mytext;

            lines = new VertexPositionColor[6];
            lines[0] = new VertexPositionColor(new Vector3(-1000, 0, 0), Color.Red);
            lines[1] = new VertexPositionColor(new Vector3(1000, 0, 0), Color.Red);
            lines[2] = new VertexPositionColor(new Vector3(0, -1000, 0), Color.Green);
            lines[3] = new VertexPositionColor(new Vector3(0, 1000, 0), Color.Green);
            lines[4] = new VertexPositionColor(new Vector3(0, 0, -1000), Color.Blue);
            lines[5] = new VertexPositionColor(new Vector3(0, 0, 1000), Color.Blue);

            */
            #endregion
        }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState state = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();


            effectiveMousePos = Vector2.Transform(mouseState.Position.ToVector2(), Matrix.CreateScale(1 / cam.Zoom));

            #region 3d Model Rotation
            float dx = 0, dy = 0, dz = 0;
            if (state.IsKeyDown(Keys.A) )
                dx -= 0.02f;
            if (state.IsKeyDown(Keys.D) )
                dx += 0.02f;
            if (state.IsKeyDown(Keys.W) )
                dy -= 0.02f;
            if (state.IsKeyDown(Keys.S) )
                dy += 0.02f;
            if (state.IsKeyDown(Keys.Q) )
                dz -= 0.02f;
            if (state.IsKeyDown(Keys.E) )
                dz += 0.02f;
            viewMatrix = Matrix.CreateRotationX(dx * MathHelper.PiOver4) * Matrix.CreateRotationY(dy * MathHelper.PiOver4) * Matrix.CreateRotationZ(dz * MathHelper.PiOver4) * viewMatrix;
            #endregion 3d Model Rotation

            if (state.IsKeyDown(Keys.G) && !oldKBState.IsKeyDown(Keys.G))
                gameDataMap.DrawGrid = !gameDataMap.DrawGrid;
            if (state.IsKeyDown(Keys.T) && !oldKBState.IsKeyDown(Keys.T))
                gameDataMap.DrawTerrainObjects = !gameDataMap.DrawTerrainObjects;
            if (state.IsKeyDown(Keys.O) && !oldKBState.IsKeyDown(Keys.O))
                myForm.Visible = !myForm.Visible;

            if (mouseState.LeftButton == ButtonState.Pressed && oldMState.LeftButton == ButtonState.Released)
                gameDataMap.MouseClick("leftdown", drawWindow, effectiveMousePos.ToPoint());
            else if (mouseState.RightButton == ButtonState.Pressed && oldMState.RightButton == ButtonState.Released)
                gameDataMap.MouseClick("rightdown", drawWindow, effectiveMousePos.ToPoint());
            else if (mouseState.MiddleButton == ButtonState.Pressed && oldMState.MiddleButton == ButtonState.Released)
                gameDataMap.MouseClick("middledown", drawWindow, effectiveMousePos.ToPoint());
            if (mouseState.LeftButton == ButtonState.Released && oldMState.LeftButton == ButtonState.Pressed)
                gameDataMap.MouseClick("leftup", drawWindow, effectiveMousePos.ToPoint());
            else if (mouseState.RightButton == ButtonState.Released && oldMState.RightButton == ButtonState.Pressed)
                gameDataMap.MouseClick("rightup", drawWindow, effectiveMousePos.ToPoint());
            else if (mouseState.MiddleButton == ButtonState.Released && oldMState.MiddleButton == ButtonState.Pressed)
                gameDataMap.MouseClick("middleup", drawWindow, effectiveMousePos.ToPoint());

            if (mouseState.MiddleButton == ButtonState.Pressed)
            {
                Vector2 deltaMouse = (mouseState.Position - oldMState.Position).ToVector2();
                cam.Pos -= deltaMouse;
            }


            if (state.IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            if (state.IsKeyDown(Keys.PageDown) && !oldKBState.IsKeyDown(Keys.PageDown))
            {
                NextFile();
                LoadMap();
            }
            else if (state.IsKeyDown(Keys.PageUp) && !oldKBState.IsKeyDown(Keys.PageUp))
            {
                PreviousFile();
                LoadMap();
            }
            

            if (state.IsKeyDown(Keys.Right))
                cam.Pos += new Vector2(50,0);
            if (state.IsKeyDown(Keys.Left))
                cam.Pos += new Vector2(-50, 0);
            if (state.IsKeyDown(Keys.Up))
                cam.Pos += new Vector2(0, -50);
            if (state.IsKeyDown(Keys.Down))
                cam.Pos += new Vector2(0, 50);

            if (cam.Pos.X < 0)
                cam.Pos = new Vector2(0, cam.Pos.Y);
            if (cam.Pos.Y < 0)
                cam.Pos = new Vector2(cam.Pos.X, 0);

            if (mouseState.ScrollWheelValue < previousScrollValue)
            {
                cam.Zoom *= 1.01f;
            }
            else if (mouseState.ScrollWheelValue > previousScrollValue)
            {
                cam.Zoom *= 1.0f/1.01f;
            }
            previousScrollValue = mouseState.ScrollWheelValue;

            oldMState = mouseState;
            oldKBState = state;
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            float scale = cam.Zoom;
            GraphicsDevice.Clear(Color.Black);
            
            //spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.get_transformation(GraphicsDevice));
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, null, null, null, null,
                Matrix.CreateScale(scale));
            overlay.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);



            Vector2 realWindow = new Vector2(Window.ClientBounds.Width, Window.ClientBounds.Height);
            Vector2 effectiveWindow = Vector2.Transform(realWindow, Matrix.CreateScale(1/scale));
            drawWindow = new Rectangle(new Point((int)cam.Pos.X, (int)cam.Pos.Y), new Point((int)effectiveWindow.X, (int)effectiveWindow.Y));


            gameDataMap.Draw(spriteBatch, drawWindow, effectiveMousePos.ToPoint());

            int frameRate = (int)(1d / gameTime.ElapsedGameTime.TotalSeconds);

            Point mouseCell = gameDataMap.Bg2Cell(new Point((int)effectiveMousePos.X + drawWindow.Location.X, (int)effectiveMousePos.Y + drawWindow.Location.Y));

            overlay.DrawString(defaultFont, string.Format("Fps: {0} Mouse: {1}, MouseCell:{3}, DrawWindow:{2}", frameRate, oldMState.Position, drawWindow, mouseCell), new Vector2(5,5), Color.Yellow);




            gridOverlay.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null,
                Matrix.CreateScale(scale));

            //if (drawGrid)
            //    gameDataMap.DrawGrid(gridOverlay, cam.Pos, effectiveWindow, Mouse.GetState().Position);


            spriteBatch.End();
            overlay.End();
            gridOverlay.End();
            

            /* 3D Modeling Experiment
            // The assignment of effect.View and effect.Projection
            // are nearly identical to the code in the Model drawing code.
            baseEffect.World = Matrix.Identity;
            baseEffect.View = viewMatrix;
            baseEffect.Projection = projectionMatrix;
            baseEffect.TextureEnabled = true;

            RasterizerState rastState = new RasterizerState();
            rastState.CullMode = CullMode.CullClockwiseFace;
            rastState.FillMode = FillMode.Solid;
            GraphicsDevice.RasterizerState = rastState;
            
            foreach (var pass in baseEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                GraphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, lines, 0, 3);
                //GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, (int)myPhy.indiceBuffer.Length);
            }
            myPhy.NextFrame(1);
            myPhy.Calculate();
            myPhy.DrawNormal(GraphicsDevice, baseEffect);
            */
            base.Draw(gameTime);
        }
    }
}