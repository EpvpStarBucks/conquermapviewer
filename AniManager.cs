﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;
using MonoGame.Extended;

namespace COMapViewer
{
    public class Ani
    {
        public string aniKey;
        public int frameAmount;
        public string[] texturePaths;
        private Texture2D[] textures;

        public Texture2D GetTexture2D(GraphicsDevice graphicsDevice, int frame = 0)
        {
            if (textures[frame] == null)
            {
                if(!File.Exists(texturePaths[frame]))
                {
                    if (Game1.DEBUGMODE)
                        Debug.WriteLine("[Ani][GetTexture2D] Unabled to load texture as path does not exist: {0}", texturePaths[frame]);
                    return null;
                }
                if (texturePaths[frame].EndsWith("dds"))/* TODO: Load MSK files.*/
                    DDSLib.DDSFromFile(texturePaths[frame], graphicsDevice, true, out textures[frame]);
                //else
                    //Debug.WriteLine(string.Format("[Ani][GetTexture2D] Attempting to load invalid texture {0}", aniKey));
                
            }
            return textures[frame];
        }
        public Ani(int frameCount)
        {
            frameAmount = frameCount;
            texturePaths = new string[frameCount];
            textures = new Texture2D[frameCount];
        }
        public void Unload()
        {
            foreach (Texture2D text in textures)
                text.Dispose();
        }

    }
    public class AniCollection
    {
        private Dictionary<string, Ani> aniCollection;

        public AniCollection()
        {
            aniCollection = new Dictionary<string, Ani>();
        }
        public bool Add(string key, Ani value)
        {
            if (!aniCollection.ContainsKey(key))
                aniCollection.Add(key, value);
            else
            {

                if (Game1.DEBUGMODE)
                    Debug.WriteLine(string.Format("[AniManager][LoadAniFile] Collection[{0}] already contains {1}", key, value.aniKey));
                return false;
            }
            return true;
        }
        public void Unload()
        {
            foreach (Ani a in aniCollection.Values)
                a.Unload();
        }
        public Texture2D GetTexture2D(GraphicsDevice graphicsDevice, string key, int frame = 0)
        {
            if(!aniCollection.ContainsKey(key))
            {
                if (Game1.DEBUGMODE)
                    Debug.WriteLine(string.Format("[AniCollection][GetTexture2D] Collection does not contain key {0}",  key));
                return null;
            }
            return aniCollection[key].GetTexture2D(graphicsDevice, frame);
        }


    }
    public static class AniCollectionManager
    {

        private static GraphicsDevice graphicsDevice;
        private static Dictionary<string, AniCollection> aniCollection = new Dictionary<string, AniCollection>();

        public static void SetGraphicsDevice(GraphicsDevice graphDev)
        {
            graphicsDevice = graphDev;
        }

        public static bool UnloadAniFile(string file)
        {
            if(aniCollection.ContainsKey(file))
            {
                aniCollection[file].Unload();
                aniCollection.Remove(file);
                return true;
            }
            return false;
        }
        
        public static bool LoadAniFile(string file)
        {
            string filePath = Game1.BaseClientPath + file;
            if (aniCollection.ContainsKey(file))
                return true;
            if (File.Exists(filePath))
            {
                if (!aniCollection.ContainsKey(file))
                    aniCollection.Add(file, new AniCollection());

                using (TextReader reader = new StreamReader(File.OpenRead(filePath)))
                {
                    while (reader.Peek() != -1)
                    {
                        string line = reader.ReadLine();
                        if (line.StartsWith("["))
                        {
                            string aniKey = line.Replace("[", "").Replace("]", "");
                            int frameAmount = int.Parse(new Regex(@"\d+").Match(reader.ReadLine()).Value);

                            Ani tAni = new Ani(frameAmount);
                            tAni.aniKey = aniKey;
                            for (int i = 0; i < tAni.frameAmount; i++)
                            {
                                string frameLine = reader.ReadLine();
                                uint frameNum = uint.Parse(new Regex(@"(?<=^Frame)\d+").Match(frameLine).Value);
                                string ddsPath = frameLine.Split('=')[1];
                                tAni.texturePaths[frameNum] = Game1.BaseClientPath + ddsPath;
                            }
                            aniCollection[file].Add(tAni.aniKey, tAni);
                        }
                    }
                }
                return true;
            }
            return false;
        }
        public static Texture2D GetTexture2D(string file, string key, int frame = 0)
        {
            if (!aniCollection.ContainsKey(file) && !LoadAniFile(file))
            {
                if (Game1.DEBUGMODE)
                    Debug.WriteLine("[AniCollectionManager][GetTexture2D] Ani File {0} could not be loaded or doesn't exist", file);
                return null;
            }
            return aniCollection[file].GetTexture2D(graphicsDevice,key, frame);
        }
        public static Size GetTexture2DSize(string file, string key, int frame = 0)
        {
            Texture2D tmp = GetTexture2D(file, key, frame);
            if (tmp == null)
                return new Size(0, 0);
            return new Size(tmp.Width, tmp.Height);
        }
    }

}
